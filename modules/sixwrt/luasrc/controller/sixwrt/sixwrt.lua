module("luci.controller.sixwrt.sixwrt", package.seeall)

function index()
	local function Node(attrs)
		local path = attrs.path or {}
		if type(path) ~= "table" then
			path = { path }
		end
		attrs.path = nil

		local page = node(unpack(path))

		if type(page.order) == "nil" and type(attrs.order) == "nil" and #path ~= 0 then
			local parent = path
			table.remove(parent)
			parent = node(unpack(parent))
			attrs.order = parent.childtail or 0
			parent.childtail = attrs.order + 1
		end

		for k, v in pairs(attrs) do
			page[k] = v
		end

		return page
	end

	-- Redirect to SixWrt per default
	Node{
		target   = alias("sixwrt"),
		index    = false,
		subindex = true,
		lock     = true,
	}
	-- Add the SixWrt top level (right) menu
	Node{
		path     = {"sixwrt"},
		title    = "SixWrt",
		target   = alias("sixwrt", "index"),
		index    = true,
		order    = 5,
	}
	-- The welcome screen
	Node{
		path     = {"sixwrt", "index"},
		title    = "Übersicht",
		target   = template("sixwrt/index"),
	}
	-- Port management
	Node{
		path     = {"sixwrt", "ports"},
		title    = "Ports",
		target   = cbi("sixwrt/ports", {autoapply = true}),
	}
end
