local CONFIG = "sixwrt"
local CONFIG_LOG = CONFIG .. "_log"

local TRACE = true

local mac  = "00:00:00:00:00:00"
local user = nil
do
	do
		local ip = luci.ip.IPv4(luci.http.getenv("REMOTE_ADDR")):string()
		for _, arp in ipairs(luci.sys.net.arptable()) do
			if arp["IP address"] == ip then
				mac = arp["HW address"]:lower()
				break
			end
		end
		-- TODO: IPv6 neigh
		-- TODO: catch unknown
	end

	local uci = luci.model.uci.cursor()
	uci:foreach(CONFIG, "user", function(sec)
		if sec.mac and sec.mac:lower() == mac then
			user = sec.name:lower()
		end
	end)
	if not user then
		user = "mac" .. mac:gsub(":", "")
		while true do
			local dup = false
			uci:foreach(CONFIG, "user", function(sec)
				dup = dup or sec.name and sec.name:lower() == user
			end)
			if not dup then break end
			user = "usr" .. luci.sys.uniqueid(6):lower()
		end
		uci:section(CONFIG, "user", nil, {
			name = user,
			mac  = mac,
		});
		uci:save(CONFIG)
	end
end

local function wrap_uci(uci)
	return setmetatable({}, {__index = function(self, func)
		if type(uci[func]) ~= "function" then return uci[func] end
		return function(self, ...)
			local cb = rawget(self, func .. "_callback")
			if type(cb) == "function" then
				if TRACE then print(func:sub(1, 3) .. ":" .. table.concat({...}, ".")) end
				cb(uci, ...)
			end
			return uci[func](uci, ...)
		end
	end})
end

do local self = Map(CONFIG, "Einstellungen für " .. mac .. " (" .. user .. ")")

	self.uci = wrap_uci(self.uci)
	function self.uci.add_callback(uci, config, type)
	end
	function self.uci.set_callback(uci, config, section, option, value)
	end
	function self.uci.delete_callback(uci, config, section, option)
	end
	function self.uci.commit_callback(uci, config)
	end

	do local self = self:section(TypedSection, "port", "Ports", "Freigeben und sperren")
		self.template = "cbi/tblsection"

		self.anonymous = true
		self.addremove = true

		self:depends("user", user)
		self.defaults.user = user

		do local self = self:option(ListValue, "target", "Aktion")
			self:value("ACCEPT", "Zulassen (ACCEPT)")
			self:value("REJECT", "Blocken (REJECT)")
			self:value("DROP",   "Verwerfen (DROP)")
			self.default = "ALLOW"
		end

		do local self = self:option(Value, "range", "Ports")
			self.size = 5
			--function self:validate(v)
			--	return v:match("^[1-9][0-9]*(-[1-9][0-9]*)$")
			--end
		end

		do local self = self:option(ListValue, "proto", "Protokoll")
			self:value("tcp", "TCP")
			self:value("udp", "UDP")
			self:value("tcpudp", "TCP+UDP")
		end

		do local self = self:option(Value, "comment", "Kommentar")
			--self.optional = true
			--self.rmempty  = false
			--self.size = 10
		end
	end

	do local self = self:section(TypedSection, "user", "Benutzereinstellung", "Kontaktinformationen und Standardeinstellungen")
		self.anonymous = true
		self.addremove = false

		self:depends("name", user)

		do local self = self:option(Value, "contact", "E-Mail (optional)")
			self.default = ""
		end

		do local self = self:option(ListValue, "target", "Standardaktion")
			self:value("ACCEPT", "Zulassen (ACCEPT)")
			self:value("REJECT", "Blocken (REJECT)")
			self:value("DROP",   "Verwerfen (DROP)")
			self.default = "REJECT"
		end
	end

	return self
end
