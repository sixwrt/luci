m = Map("aiccu", "AICCU")

s = m:section(TypedSection, "aiccu", "Tunnel")
s.addremove = true
s.anonymous = true

o = s:option(Value, "username", "User Name")
o = s:option(Value, "password", "Password")
o.password = true

o = s:option(Value, "server",   "Server")
o = s:option(Value, "protocol", "Protocol")
o:value("tic", "TIC")
o:value("tsp", "TSP")
o:value("l2tp", "L2TP")
o.default = "tic"

o = s:option(Value, "interface", "Interface")
o.default = "aiccu"

o = s:option(Value, "tunnel_id", "Tunnel Id")
o = s:option(Flag, "requiretls", "Require TLS")
o.default = 1
o.optional = true
o:depends("protocol", "tic")

o = s:option(Flag, "defaultroute", "Add a default route")
o.default = 1
o.optional = true

o = s:option(Flag, "nat", "Behind NAT")
o.default = 1
o.optional = true

o = s:option(Flag, "heartbeat", "Send heartbeats")
o.default = 1
o.optional = true

return m
