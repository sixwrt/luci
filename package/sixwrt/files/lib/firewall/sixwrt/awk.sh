awk_call() {
	local file=$1
	local call=$2
	[ "$call" == "print" ] && {
		call="$call $3"
		shift
	}
	shift 2

	call="${call}("
	[ $# -gt 0 ] && {
		call="${call}ARGV[1]"
		[ $# -gt 1 ] && {
			local i
			for i in 2 $(seq 3 $#); do
				call="${call}, ARGV[$i]"
			done
		}
	}
	call="${call})"
	echo "BEGIN { $call }" | awk $vars -f "$file" -f - -- "$@"
}
