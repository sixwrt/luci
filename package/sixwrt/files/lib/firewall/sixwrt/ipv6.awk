function hex2dec(hex , dec, i) {
	dec = 0
	h = tolower(h);
	for (i = 1; i <= length(hex); i++) {
		dec = lshift(dec, 4) + index("0123456789abcdef", substr(hex, i, 1)) - 1
	}
	return dec
}

function ipv6_expand_mac(mac , b, i) {
	split(mac, b, ":")
	for (i in b) {
		b[i] = hex2dec(b[i])
	}
	b[1] = or(and(b[1], 0xfd), and(xor(b[1], 0xff), 0x02))
	return sprintf("%02x%02x:%02x%02x:%02x%02x:%02x%02x", b[1], b[2], b[3], 0xff, 0xfe, b[4], b[5], b[6])
}

function ipv6_expand(ip6 , w, c, i) {
	c = split(ip6, w, "/")
	c = split(w[1], w, ":")
	if (c < 8 && w[c] != "") {
		for (i = 8; i >= 1; i--) {
			if (w[c] == "") break
			w[i] = w[c]
			w[c] = ""
			c--
		}
	}
	for (i = 1; i <= 8; i++) {
		if (w[i] == "") w[i] = "0"
		w[i] = hex2dec(w[i])
	}
	return sprintf("%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x", w[1], w[2], w[3], w[4], w[5], w[6], w[7], w[8])
}

function ipv6_from_mac(pfx, mac) {
	return substr(ipv6_expand(pfx), 1, 19) ":" ipv6_expand_mac(mac)
}

function ipv6_get_addrs(iface , s) {
	s = ""
	while("ip -6 addr show dev " iface | getline) {
		if ($1 == "inet6") {
			s = s ipv6_expand($2) FS
		}
	}
	return s
}

