. ${FW_LIBDIR}/sixwrt/awk.sh


sixwrt_tunnel_load() {
	fw_config_get_section "$1" sixwrt_tunnel { \
		string prefix "" \
		string policy "REJECT" \
	}
}

sixwrt_pre_core_cb() {
	fw_config_append sixwrt
}

sixwrt_post_defaults_cb() {
	config_foreach sixwrt_tunnel_load tunnel
	[ -n "$sixwrt_tunnel_prefix" ] || return

	fw add 6 filter sixwrt_in
	fw add 6 filter sixwrt_out

	fw add 6 filter sixwrt_in  $sixwrt_tunnel_policy
	fw add 6 filter sixwrt_out ACCEPT
}

sixwrt_post_zone_cb() {
	[ -n "$sixwrt_tunnel_prefix" ] || return
	case "$zone_name" in
		int)
			fw add 6 filter zone_${zone_name}_forward sixwrt_in ^ { \
				-d "${sixwrt_tunnel_prefix}/64" \
			} ;;
		ext)
			fw add 6 filter zone_${zone_name}_forward sixwrt_out ^ { \
				-s "${sixwrt_tunnel_prefix}/64" \
			} ;;
	esac
}

sixwrt_post_core_cb() {
	[ -n "$sixwrt_tunnel_prefix" ] || return

	config_foreach sixwrt_add_user user
}


sixwrt_user_load() {
	fw_config_get_section "$1" sixwrt_user { \
		string name "" \
		string mac "" \
		string policy $sixwrt_tunnel_policy \
	}
	[ -n "${sixwrt_user_mac}" ] || return 1
	[ -n "${sixwrt_user_name}" ] || sixwrt_user_name=$sixwrt_user_mac
	sixwrt_user_name=$(echo "$sixwrt_user_name" | sed -e 's/[^a-zA-Z0-9]//g')

	sixwrt_user_chain=sixwrt_user_${sixwrt_user_name}
	sixwrt_user_ip6=$(awk_call ${FW_LIBDIR}/sixwrt/ipv6.awk print \
		ipv6_from_mac "${sixwrt_tunnel_prefix}" "${sixwrt_user_mac}")
}

sixwrt_add_user() {
	sixwrt_user_load "$1"

	fw add 6 filter ${sixwrt_user_chain}
	fw add 6 filter sixwrt_in ${sixwrt_user_chain} { \
		-d ${sixwrt_user_ip6} \
	}

	config_foreach sixwrt_do_rule port add
}

sixwrt_del_user() {
	sixwrt_user_load "$1"

	fw del 6 filter sixwrt_in ${sixwrt_user_chain} { \
		-d ${sixwrt_user_ip6} \
	}
	fw del 6 filter ${sixwrt_user_chain}
}


sixwrt_rule_load() {
	fw_config_get_section "$1" sixwrt_rule { \
		string user "" \
		string range "" \
		string proto "tcpudp" \
		string target "ACCEPT" \
	}
	[ "${sixwrt_rule_target}" == "REJECT" ] && sixwrt_rule_target=reject
}

sixwrt_do_rule() {
	sixwrt_rule_load "$1"
	local action=$2

	[ "${sixwrt_rule_user}" == "${sixwrt_user_name}" ] || return
	[ -n "${sixwrt_rule_range}" ] || return 1

	[ "$sixwrt_rule_proto" == "tcpudp" ] && sixwrt_rule_proto="tcp udp"
	for sixwrt_rule_proto in $sixwrt_rule_proto; do
		fw $action 6 filter ${sixwrt_user_chain} ${sixwrt_rule_target} { \
			-p "$sixwrt_rule_proto" \
			--dport $(echo ${sixwrt_rule_range} | sed -e 's/-/:/') \
		}
	done
}
