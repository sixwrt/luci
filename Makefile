BRANCH:=branches/luci-0.9
SOURCE_URL:=http://svn.luci.subsignal.org/luci/$(BRANCH)

LUCIDIR=lucisrc

.PHONY: all build links update veryclean debian-deps

all: links build

links: build/config.mk
	for i in applications modules themes; do \
	  for j in $$i/*; do \
	    test -d $$j && ln -sf ../../$$j $(LUCIDIR)/$$i; \
	  done \
	done || true
	for j in package/*; do \
	  test -d $$j && ln -sf ../../../$$j $(LUCIDIR)/contrib/$$j; \
	done

update: $(LUCIDIR)/Makefile
	cd $(LUCIDIR) && svn up

$(LUCIDIR)/Makefile:
	svn co -q $(SOURCE_URL) $(LUCIDIR)

build/config.mk:
	ln -sf $(LUCIDIR)/build build


build %: $(LUCIDIR)/Makefile build/config.mk
	$(MAKE) -C $(LUCIDIR) $@

veryclean:
	rm -fr $(LUCIDIR)
	rm -f build

# http://luci.freifunk-halle.net/Documentation/DevelopmentEnvironmentHowTo
debian-deps:
	sudo aptitude install \
		libiw-dev \
		lua5.1 \
		liblua5.1-dev

